package com.timeconqueror.enigmatics.registry;

import com.timeconqueror.enigmatics.CreativeTabE;
import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.common.items.ItemAncientTablet;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModItems {
    public static ItemAncientTablet ancientTablet = new ItemAncientTablet();
    public static Item illuriteCrystal = new Item();


    public static void register(){
        registerItem(ancientTablet, "ancient_tablet");
        registerItem(illuriteCrystal, "illurite_crystal");
    }

    @SideOnly(Side.CLIENT)
    public static void registerRenderers(){
        registerItemRender(ancientTablet);
        registerItemRender(illuriteCrystal);
    }

    private static void registerItem(Item item, String name) {
        ForgeRegistries.ITEMS.register(item.setTranslationKey(name).setRegistryName(new ResourceLocation(Enigmatics.MODID, name)).setCreativeTab(CreativeTabE.enigmatics));
    }

    @SideOnly(Side.CLIENT)
    private static void registerItemRender(Item item, int meta, String fileName) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(fileName, "inventory"));
    }

    @SideOnly(Side.CLIENT)
    private static void registerItemRender(Item item){
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}
