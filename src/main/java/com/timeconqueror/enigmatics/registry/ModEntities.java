package com.timeconqueror.enigmatics.registry;

import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.client.render.entities.RenderPhoenix;
import com.timeconqueror.enigmatics.common.entities.EntityPhoenix;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class ModEntities {
    public static void register() {
        int id = 0;
        //EntityTracker
        registerEntity("phoenix", EntityPhoenix.class, id++, 80, 3, true, 0xe2bf0d, 0xed6c09);

    }

    public static void registerRenders() {
        RenderingRegistry.registerEntityRenderingHandler(EntityPhoenix.class, RenderPhoenix::new);
    }

    public static void registerEntity(String name, Class<? extends Entity> entityClass, int id, int trackingRange, int updateFrequency, boolean sendVelocityUpdates) {
        EntityRegistry.registerModEntity(new ResourceLocation(Enigmatics.MODID + ":" + name), entityClass, name, id, Enigmatics.instance, trackingRange, updateFrequency, sendVelocityUpdates);
    }

    public static void registerEntity(String name, Class<? extends Entity> entityClass, int id, int trackingRange, int updateFrequency, boolean sendVelocityUpdates, int primaryEggColor, int secondaryEggColor) {
        EntityRegistry.registerModEntity(new ResourceLocation(Enigmatics.MODID + ":" + name), entityClass, name, id, Enigmatics.instance, trackingRange, updateFrequency, sendVelocityUpdates, primaryEggColor, secondaryEggColor);
    }
}
