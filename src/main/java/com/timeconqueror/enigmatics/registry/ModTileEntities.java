package com.timeconqueror.enigmatics.registry;

import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.common.tileentitites.TESR.TESRTest;
import com.timeconqueror.enigmatics.common.tileentitites.TETest;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModTileEntities {
    public static void register() {
        GameRegistry.registerTileEntity(TETest.class, new ResourceLocation(Enigmatics.MODID, "test_block"));
    }

    @SideOnly(Side.CLIENT)
    public static void registerTESRs() {
        ClientRegistry.bindTileEntitySpecialRenderer(TETest.class, new TESRTest());
    }
}
