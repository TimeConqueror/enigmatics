package com.timeconqueror.enigmatics.registry;

import com.timeconqueror.enigmatics.CreativeTabE;
import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.common.blocks.BlockBase;
import com.timeconqueror.enigmatics.common.blocks.BlockIlluriteOre;
import com.timeconqueror.enigmatics.common.blocks.BlockTest;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModBlocks {
    public static BlockIlluriteOre illuriteOre = new BlockIlluriteOre();
    public static BlockBase fariumStone = new BlockBase(Material.ROCK, "pickaxe", 3);
    public static BlockBase spaceLamp = new BlockBase(Material.GLASS, "pickaxe", 2, 1.0F);
    public static BlockTest testBlock = new BlockTest(Material.ROCK);

    public static void register() {
        registerBlock(illuriteOre, "illurite_ore", new ItemBlock(illuriteOre));
        registerBlock(fariumStone, "farium_stone", new ItemBlock(fariumStone));
        registerBlock(spaceLamp, "space_lamp", new ItemBlock(spaceLamp));
        registerBlock(testBlock, "test_block", new ItemBlock(testBlock));
    }

    @SideOnly(Side.CLIENT)
    public static void registerRenderers() {
        registerBlockRender(illuriteOre);
        registerBlockRender(fariumStone);
        registerBlockRender(spaceLamp);
        registerBlockRender(testBlock);
    }

    private static void registerBlock(Block block, String name) {
        ForgeRegistries.BLOCKS.register(block.setTranslationKey(name).setRegistryName(Enigmatics.MODID, name));
    }

    private static void registerBlock(Block block, String name, ItemBlock itemBlock) {
        ForgeRegistries.BLOCKS.register(block.setTranslationKey(name).setRegistryName(Enigmatics.MODID, name).setCreativeTab(CreativeTabE.enigmatics));
        ForgeRegistries.ITEMS.register(itemBlock.setRegistryName(Enigmatics.MODID, name));
    }

    @SideOnly(Side.CLIENT)
    private static void registerBlockRender(Block block) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(new ResourceLocation(Enigmatics.MODID, block.getTranslationKey().substring(5)), "inventory"));
    }
}
