package com.timeconqueror.enigmatics.registry;

import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.api.tablet.TabletAPI;
import com.timeconqueror.enigmatics.api.tablet.TabletCategory;
import com.timeconqueror.enigmatics.api.tablet.TabletEntry;
import com.timeconqueror.enigmatics.api.tablet.desccomponents.IDescComponent;
import com.timeconqueror.enigmatics.api.tablet.desccomponents.StringDescComponent;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import java.util.ArrayList;
import java.util.List;

public class TabletRegistry {

    private static final ResourceLocation BASICS = new ResourceLocation(Enigmatics.MODID, "textures/gui/bg1.png");

    public static void register() {

        TabletAPI.registerCategory(new TabletCategory("BASICS", BASICS));
        TabletAPI.registerCategory(new TabletCategory("TEST", BASICS));

        List<IDescComponent> desc = new ArrayList<>();

        desc.add(new StringDescComponent("Переночевав в гостинице в Гуаякиле, мы сели к агенту в машину и поехали на судно в Пуэрто Боливар. Доехали вопреки ожиданиям быстро, примерно за 3-4 часа. Погода была пасмурная и даже не смотря на то, что мы находимся недалеко от экватора, было прохладно. Почти все время, пока мы ехали, по обе стороны дороги были банановые плантации, но все равно в голове не укладывается: эти бананы грузят на суда в нескольких портах Эквадора десятками тысяч тонн каждый день, круглый год. Это ж несчастные бананы должны расти быстрее чем грибы."));
        desc.add(new StringDescComponent("недалеко от экватора, было прохладно. Почти все время, пока мы ехали, по обе стороны дороги были банановые плантации, но все равно в голове не укладывается: эти бананы грузят на суда в нескольких портах Эквадора десятками тысяч тонн каждый день, круглый год. Это ж несчастные бананы должны расти быстрее чем грибы."));
        desc.add(new StringDescComponent("Переночевав в гостинице в Гуаякиле, мы сели к агенту в машину и поехали на судно в Пуэрто Боливар. Доехали вопреки ожиданиям быстро, примерно за 3-4 часа. Погода была пасмурная и даже не смотря на то, что мы находимся недалеко от экватора, было прохладно. Почти все время, пока мы ехали, по обе стороны дороги были банановые плантации, но все равно в голове не укладывается: эти бананы грузят на суда в нескольких портах Эквадора десятками тысяч тонн каждый день, круглый год. Это ж несчастные бананы должны расти быстрее чем грибы."));

        for (int i = 0; i < 3; i++)
            TabletAPI.registerEntry("BASICS", new TabletEntry("Test Entry" + i, "Jocky joke", new ItemStack(Blocks.BARRIER), desc, 10 + i * 10, 10 + i * 10));
        TabletAPI.registerEntry("TEST", new TabletEntry("Test Entry_", "Jocky joke", new ItemStack(Blocks.BRICK_BLOCK), desc, 10 + 10, 10 + 10));
        desc.clear();
    }
}
