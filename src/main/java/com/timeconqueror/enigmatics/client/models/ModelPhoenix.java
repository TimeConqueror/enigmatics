package com.timeconqueror.enigmatics.client.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModelPhoenix extends ModelBase {
    ModelRenderer body;


    public ModelPhoenix() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.body = new ModelRenderer(this, 0, 0);
        this.body.setRotationPoint(0.003203F, -2.61405F, 14.5456F);
        this.body.addBox(0.0F, 0.0F, 0.0F, 121212121, 1, 1, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        this.body.render(scale);
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
//    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
//    {
//        float f = ageInTicks * 0.3F;
//        this.head.rotateAngleX = headPitch * 0.017453292F;
//        this.head.rotateAngleY = netHeadYaw * 0.017453292F;
//        this.head.rotateAngleZ = 0.0F;
//        this.head.rotationPointX = 0.0F;
//        this.body.rotationPointX = 0.0F;
//        this.tail.rotationPointX = 0.0F;
//        this.wingRight.rotationPointX = -1.5F;
//        this.wingLeft.rotationPointX = 1.5F;
//
//        if (this.state != ModelPhoenix.State.FLYING)
//        {
//            if (this.state == ModelPhoenix.State.SITTING)
//            {
//                return;
//            }
//
//            if (this.state == ModelPhoenix.State.PARTY)
//            {
//                float f1 = MathHelper.cos((float)entityIn.ticksExisted);
//                float f2 = MathHelper.sin((float)entityIn.ticksExisted);
//                this.head.rotationPointX = f1;
//                this.head.rotationPointY = 15.69F + f2;
//                this.head.rotateAngleX = 0.0F;
//                this.head.rotateAngleY = 0.0F;
//                this.head.rotateAngleZ = MathHelper.sin((float)entityIn.ticksExisted) * 0.4F;
//                this.body.rotationPointX = f1;
//                this.body.rotationPointY = 16.5F + f2;
//                this.wingLeft.rotateAngleZ = -0.0873F - ageInTicks;
//                this.wingLeft.rotationPointX = 1.5F + f1;
//                this.wingLeft.rotationPointY = 16.94F + f2;
//                this.wingRight.rotateAngleZ = 0.0873F + ageInTicks;
//                this.wingRight.rotationPointX = -1.5F + f1;
//                this.wingRight.rotationPointY = 16.94F + f2;
//                this.tail.rotationPointX = f1;
//                this.tail.rotationPointY = 21.07F + f2;
//                return;
//            }
//
//            this.legLeft.rotateAngleX += MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
//            this.legRight.rotateAngleX += MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
//        }
//
//        this.head.rotationPointY = 15.69F + f;
//        this.tail.rotateAngleX = 1.015F + MathHelper.cos(limbSwing * 0.6662F) * 0.3F * limbSwingAmount;
//        this.tail.rotationPointY = 21.07F + f;
//        this.body.rotationPointY = 16.5F + f;
//        this.wingLeft.rotateAngleZ = -0.0873F - ageInTicks;
//        this.wingLeft.rotationPointY = 16.94F + f;
//        this.wingRight.rotateAngleZ = 0.0873F + ageInTicks;
//        this.wingRight.rotationPointY = 16.94F + f;
//        this.legLeft.rotationPointY = 22.0F + f;
//        this.legRight.rotationPointY = 22.0F + f;
//    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
//    public void setLivingAnimations(EntityLivingBase entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTickTime)
//    {
//        this.feather.rotateAngleX = -0.2214F;
//        this.body.rotateAngleX = 0.4937F;
//        this.wingLeft.rotateAngleX = -((float)Math.PI * 2F / 9F);
//        this.wingLeft.rotateAngleY = -(float)Math.PI;
//        this.wingRight.rotateAngleX = -((float)Math.PI * 2F / 9F);
//        this.wingRight.rotateAngleY = -(float)Math.PI;
//        this.legLeft.rotateAngleX = -0.0299F;
//        this.legRight.rotateAngleX = -0.0299F;
//        this.legLeft.rotationPointY = 22.0F;
//        this.legRight.rotationPointY = 22.0F;
//
//        if (entitylivingbaseIn instanceof EntityParrot)
//        {
//            EntityParrot entityparrot = (EntityParrot)entitylivingbaseIn;
//
//            if (entityparrot.isPartying())
//            {
//                this.legLeft.rotateAngleZ = -0.34906584F;
//                this.legRight.rotateAngleZ = 0.34906584F;
//                this.state = ModelPhoenix.State.PARTY;
//                return;
//            }
//
//            if (entityparrot.isSitting())
//            {
//                float f = 1.9F;
//                this.head.rotationPointY = 17.59F;
//                this.tail.rotateAngleX = 1.5388988F;
//                this.tail.rotationPointY = 22.97F;
//                this.body.rotationPointY = 18.4F;
//                this.wingLeft.rotateAngleZ = -0.0873F;
//                this.wingLeft.rotationPointY = 18.84F;
//                this.wingRight.rotateAngleZ = 0.0873F;
//                this.wingRight.rotationPointY = 18.84F;
//                ++this.legLeft.rotationPointY;
//                ++this.legRight.rotationPointY;
//                ++this.legLeft.rotateAngleX;
//                ++this.legRight.rotateAngleX;
//                this.state = ModelPhoenix.State.SITTING;
//            }
//            else if (entityparrot.isFlying())
//            {
//                this.legLeft.rotateAngleX += ((float)Math.PI * 2F / 9F);
//                this.legRight.rotateAngleX += ((float)Math.PI * 2F / 9F);
//                this.state = ModelPhoenix.State.FLYING;
//            }
//            else
//            {
//                this.state = ModelPhoenix.State.STANDING;
//            }
//
//            this.legLeft.rotateAngleZ = 0.0F;
//            this.legRight.rotateAngleZ = 0.0F;
//        }
//    }

    @SideOnly(Side.CLIENT)
    static enum State {
        FLYING,
        STANDING,
        SITTING,
        PARTY
    }
}
