package com.timeconqueror.enigmatics.client.render.blockoverlays;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ModelOverlay extends ModelBase {

    public ModelRenderer cube = (new ModelRenderer(this, 0, 0)).setTextureSize(64, 64);

    public ModelOverlay() {
        this.cube.addBox(-0.005F, -0.005F, -0.005F, 16, 16, 16, 0.0F);
    }

    public void render() {
        this.cube.render(0.0625390625F);
    }
}
