package com.timeconqueror.enigmatics.client.render.blockoverlays;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@SideOnly(Side.CLIENT)
public class BlockOverlaysManager {

    private static Minecraft mc = Minecraft.getMinecraft();
    private static HashMap<BlockPos, BlockOverlay> overlays = new HashMap<>();

    private static boolean enabled;

    public static void add(BlockOverlay overlay) {
        if (!overlays.containsKey(overlay.getPos())) {
            overlays.put(overlay.getPos(), overlay);
        }
    }

    public static void onRender(World world, float partialTicks) {
        enabled = true;
        if (enabled) {

            for (BlockOverlay overlay : overlays.values()) {
                overlay.onRender(world, partialTicks);
            }
        }
    }

    public static void onUpdate() {
        if (enabled) {
            if (mc.world == null) {
                enabled = false;
                clear();
            } else {
                Iterator<Map.Entry<BlockPos, BlockOverlay>> iter = overlays.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry<BlockPos, BlockOverlay> entry = iter.next();
                    if (entry.getValue().isValid()) {
                        entry.getValue().onUpdate();
                    } else {
                        iter.remove();
                    }
                }
            }
        } else if (mc.world != null) {
            enabled = true;
        }
    }

    public static void clear() {
        overlays.clear();
    }

    @SubscribeEvent
    public void onPlayerChangeWorld(PlayerEvent.PlayerChangedDimensionEvent event) {
        clear();
    }

    @SubscribeEvent
    public void onUpdateClient(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.START) {
            onUpdate();
        }
    }

    @SubscribeEvent
    public void onRender(RenderWorldLastEvent event) {
        onRender(Minecraft.getMinecraft().world, event.getPartialTicks());
    }

    @SubscribeEvent
    public void onBlockBreak(BlockEvent.BreakEvent event) {
        if (overlays.containsKey(event.getPos())) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                overlays.remove(event.getPos());
            });
        }
    }

    @SubscribeEvent
    public void onChunkUnload(ChunkEvent.Unload event) {
        Minecraft.getMinecraft().addScheduledTask(() -> {
            overlays.entrySet().removeIf(entry -> event.getChunk().isAtLocation(entry.getKey().getX() / 16, entry.getKey().getZ() / 16));
        });
    }

    @SubscribeEvent
    public void onBlockUpdate(BlockEvent.NeighborNotifyEvent event) {
        BlockOverlay overlay = overlays.get(event.getPos());
        if (overlay != null && overlay.getBlock() != event.getState().getBlock()) {
            Minecraft.getMinecraft().addScheduledTask(() -> {
                overlays.remove(event.getPos());
            });
        }
    }
}
