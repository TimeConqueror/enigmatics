package com.timeconqueror.enigmatics.client.render.entities;

import com.timeconqueror.enigmatics.Enigmatics;
import com.timeconqueror.enigmatics.client.models.ModelPhoenix;
import com.timeconqueror.enigmatics.common.entities.EntityPhoenix;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

public class RenderPhoenix extends RenderLiving<EntityPhoenix> {

    public RenderPhoenix(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new ModelPhoenix(), 0.5F);
    }

    @Override
    protected void applyRotations(EntityPhoenix entityLiving, float p_77043_2_, float rotationYaw, float partialTicks) {
        super.applyRotations(entityLiving, p_77043_2_, rotationYaw, partialTicks);
    }

    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(EntityPhoenix entity) {
        return new ResourceLocation(Enigmatics.MODID, "textures/entity/phoenix.png");
    }
}
