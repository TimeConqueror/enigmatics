package com.timeconqueror.enigmatics.client.render.blockoverlays;

import com.timeconqueror.enigmatics.Enigmatics;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockOverlay {

    private static ModelOverlay modelOverlay = new ModelOverlay();
    private static ResourceLocation resource = new ResourceLocation(Enigmatics.MODID, "textures/misc/illurite_ore_overlay.png");

    private Block block;
    private BlockPos pos;
    private int ticks;

    public BlockOverlay(Block block, BlockPos pos) {
        this.block = block;
        this.pos = new BlockPos(pos.getX(), pos.getY(), pos.getZ());
    }

    public void onRender(World world, float partialTicks) {
        RenderManager rm = Minecraft.getMinecraft().getRenderManager();

        GlStateManager.pushMatrix();

        GlStateManager.translate(pos.getX() - rm.viewerPosX, pos.getY() - rm.viewerPosY + 1, pos.getZ() - rm.viewerPosZ + 1);
        GlStateManager.scale(1.0D, -1.0D, -1.0D);
        Minecraft.getMinecraft().getTextureManager().bindTexture(resource);
        modelOverlay.render();

        GlStateManager.popMatrix();
    }

    public void onUpdate() {
        ticks++;
    }

    public int getTickExists() {
        return ticks;
    }

    public BlockPos getPos() {
        return pos;
    }

    public Block getBlock() {
        return block;
    }

    public boolean isValid() {
        return true;
    }
}
