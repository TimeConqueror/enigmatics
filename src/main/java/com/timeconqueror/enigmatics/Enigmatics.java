package com.timeconqueror.enigmatics;

import com.timeconqueror.enigmatics.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Enigmatics.MODID, name = Enigmatics.NAME, version = Enigmatics.VERSION)
public class Enigmatics {

    public static final String MODID = "enigmatics";
    public static final String NAME = "Enigmatics";
    public static final String VERSION = "1.0";

    public static Logger logger = LogManager.getLogger("Enigmatics");

    @SidedProxy(clientSide = "com.timeconqueror.enigmatics.proxy.ClientProxy", serverSide = "com.timeconqueror.enigmatics.proxy.ServerProxy")
    public static CommonProxy proxy;

    @Mod.Instance
    public static Enigmatics instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
}
