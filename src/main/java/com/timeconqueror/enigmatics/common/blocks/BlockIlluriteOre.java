package com.timeconqueror.enigmatics.common.blocks;

import com.timeconqueror.enigmatics.client.render.blockoverlays.BlockOverlay;
import com.timeconqueror.enigmatics.client.render.blockoverlays.BlockOverlaysManager;
import com.timeconqueror.enigmatics.registry.ModItems;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class BlockIlluriteOre extends Block {

    public BlockIlluriteOre() {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHarvestLevel("pickaxe", 3);
        setHardness(3.0f);
        setResistance(5.0f);
    }


    @Override
    public Item getItemDropped(IBlockState state, Random random, int fortune) {
        return ModItems.illuriteCrystal;
    }

    @Override
    public int quantityDropped(Random random) {
        return 1 + random.nextInt(4);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        BlockOverlaysManager.add(new BlockOverlay(this, pos) {
            @Override
            public boolean isValid() {
                return true; // true - если ночь
            }
        });
    }
}
