package com.timeconqueror.enigmatics.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockBase extends Block {

    public BlockBase(Material materialIn, String harvestTool, int harvestLevel) {
        super(materialIn);
        setSoundType(SoundType.STONE);
        setHarvestLevel(harvestTool, harvestLevel);
    }

    public BlockBase(Material materialIn, String harvestTool, int harvestLevel, float lightLevel) {
        super(materialIn);
        setSoundType(SoundType.STONE);
        setHarvestLevel(harvestTool, harvestLevel);
        setLightLevel(lightLevel);
    }
}
