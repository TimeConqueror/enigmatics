package com.timeconqueror.enigmatics.common.world;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.HashMap;

public class StructureBase {

    private ArrayList<String[]> layers = new ArrayList<>();
    private HashMap<Character, IBlockState> blocks = new HashMap<>();
    /**
     * While writing the layer array to add it  to the method this will represent horizontal axis while looking from above (the length of every string line in every layer)
     */
    private int length;
    /**
     * While writing the layer array to add it to the method this will represent vertical axis while looking from above (the size of layers ArrayList)
     */
    private int width;

    /**
     * Remark: Height is not important. it will be determined later.
     */
    public StructureBase(int width, int length) {
        this.width = width;
        this.length = length;
    }

    /**
     * Adds layer (horizontal rectangle with height in one block) to specify the location of blocks when generating them
     */
    public StructureBase addLayer(String[] layer) {
        int iterator = 0;
        for (String strip : layer) {
            if (strip.length() > length) {
                throw new IllegalArgumentException("The string " + iterator + " of layer " + (layers.size() + 1) + "is bigger than structure length (" + length + ")!");
            }
            iterator++;
        }

        if (layer.length > width) {
            throw new IllegalArgumentException("The layer " + (layers.size() + 1) + "is bigger than structure width (" + width + ")!");
        }

        layers.add(layer);
        return this;
    }

    /**
     * Starts the generation. Must be called for generation.
     */
    public void generateIn(World world, int x, int y, int z) {
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                for (int k = 0; k < getLength(); k++) {
                    char mappingKey = layers.get(i)[j].toCharArray()[k];
                    //centerCoordX                    centerCoordZ
                    placeBlock(world, new BlockPos(x + j - width / 2, y + i, z + k - length / 2), mappingKey == ' ' ? Blocks.AIR.getDefaultState() : blocks.get(mappingKey), mappingKey);
                }
            }
        }
    }

    /**
     * Places the block in the world.
     */
    public void placeBlock(World world, BlockPos pos, IBlockState state, char mappingKey) {
        if (!(mappingKey == ' ')) {
            world.setBlockState(pos, state, 2 | 16);
        }
    }

    /**
     * Adds Block to List for future layer-building.
     * If name = " ", this will be a block, that which will not be affected during structure generating.
     */
    public StructureBase addBlockMapping(char name, IBlockState blockState) {
        blocks.put(name, blockState);
        return this;
    }

    /**
     * Returns the width of the structure
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the length of the structure
     */
    public int getLength() {
        return length;
    }

    /**
     * Returns the height of the structure
     */
    public int getHeight() {
        return layers.size();
    }
}
