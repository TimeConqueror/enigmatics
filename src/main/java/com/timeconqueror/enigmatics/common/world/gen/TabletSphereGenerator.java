package com.timeconqueror.enigmatics.common.world.gen;

import com.timeconqueror.enigmatics.common.world.StructureBase;
import com.timeconqueror.enigmatics.registry.ModBlocks;
import com.timeconqueror.enigmatics.utilities.CommonUtils;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockStainedGlass;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class TabletSphereGenerator extends StructureBase implements IWorldGenerator {
    public TabletSphereGenerator() {
        super(9, 9);

        addBlockMapping('S', ModBlocks.fariumStone.getDefaultState());
        addBlockMapping('L', ModBlocks.spaceLamp.getDefaultState());
        addBlockMapping('G', Blocks.STAINED_GLASS.getDefaultState().withProperty(BlockStainedGlass.COLOR, EnumDyeColor.BLUE));
        addBlockMapping('C', Blocks.CHEST.getDefaultState().withProperty(BlockChest.FACING, EnumFacing.NORTH));
        addBlockMapping('A', Blocks.AIR.getDefaultState());

        addLayer(new String[]{
                "         ",
                "         ",
                "         ",
                "   SSS   ",
                "   SSS   ",
                "   SSS   ",
                "         ",
                "         ",
                "         "
        }).addLayer(new String[]{
                "         ",
                "         ",
                "   SSS   ",
                "  SLLLS  ",
                "  SLLLS  ",
                "  SLLLS  ",
                "   SSS   ",
                "         ",
                "         "
        }).addLayer(new String[]{
                "         ",
                "   SSS   ",
                "  SSSSS  ",
                " SSGGGSS ",
                " SSGGGSS ",
                " SSGGGSS ",
                "  SSSSS  ",
                "   SSS   ",
                "         "
        }).addLayer(new String[]{
                "   SSS   ",
                "  SSSSS  ",
                " SSSSSSS ",
                "SSSAAASSS",
                "SSSACASSS",
                "SSSAAASSS",
                " SSSSSSS ",
                "  SSSSS  ",
                "   SSS   "
        }).addLayer(new String[]{
                "   SSS   ",
                "  SSSSS  ",
                " SSSSSSS ",
                "SSSAAASSS",
                "SSSAAASSS",
                "SSSAAASSS",
                " SSSSSSS ",
                "  SSSSS  ",
                "   SSS   "
        }).addLayer(new String[]{
                "   SSS   ",
                "  SSSSS  ",
                " SSSSSSS ",
                "SSSAAASSS",
                "SSSAAASSS",
                "SSSAAASSS",
                " SSSSSSS ",
                "  SSSSS  ",
                "   SSS   "
        }).addLayer(new String[]{
                "         ",
                "   SSS   ",
                "  SSSSS  ",
                " SSSSSSS ",
                " SSSSSSS ",
                " SSSSSSS ",
                "  SSSSS  ",
                "   SSS   ",
                "         "
        }).addLayer(new String[]{
                "         ",
                "         ",
                "   SSS   ",
                "  SSSSS  ",
                "  SSSSS  ",
                "  SSSSS  ",
                "   SSS   ",
                "         ",
                "         "
        }).addLayer(new String[]{
                "         ",
                "         ",
                "         ",
                "   SSS   ",
                "   SSS   ",
                "   SSS   ",
                "         ",
                "         ",
                "         "
        });
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (world.provider.getDimension() == DimensionType.OVERWORLD.getId() && !world.isRemote) {
//            int xx = chunkX * 16 + 5 + random.nextInt(2);
//            int zz = chunkZ * 16 + 5 + random.nextInt(2);
            int xx = chunkX * 16 + random.nextInt(6);
            int zz = chunkZ * 16 + random.nextInt(6);

            int chance = 800;
            if (random.nextInt(chance) == 0) {
                System.out.println(xx + " " + zz);
                int height = chunkProvider.provideChunk(chunkX, chunkZ).getLowestHeight() - 5;
                if (height > 0) {
                    if (!BiomeManager.oceanBiomes.contains(world.getBiome(new BlockPos(zz, height, zz))) &&
                            !BiomeManager.oceanBiomes.contains(world.getBiome(new BlockPos(chunkX * 16, height, chunkZ * 16 + 15))) &&
                            !BiomeManager.oceanBiomes.contains(world.getBiome(new BlockPos(chunkX * 16 + 15, height, chunkZ * 16))) &&
                            !BiomeManager.oceanBiomes.contains(world.getBiome(new BlockPos(chunkX * 16 + 15, height, chunkZ * 16 + 15)))) {
                        this.generateIn(world, xx, height, zz);
                    }
                }
            }
        }
    }

    @Override
    public void placeBlock(World world, BlockPos pos, IBlockState state, char mappingKey) {
        super.placeBlock(world, pos, state, mappingKey);

        if (state.getBlock() == Blocks.CHEST) {
            if (world.getTileEntity(pos) != null) {
                TileEntityChest chest = (TileEntityChest) world.getTileEntity(pos);
                chest.setLootTable(LootTableList.CHESTS_SIMPLE_DUNGEON, CommonUtils.rand.nextLong());
                chest.markDirty();
                world.notifyBlockUpdate(pos, state, state, 3);
            }
        }
    }
}
