package com.timeconqueror.enigmatics.common.entities;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.passive.EntityFlying;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class EntityPhoenix extends EntityTameable implements EntityFlying {

    public EntityPhoenix(World worldIn) {
        super(worldIn);
    }

    @Nullable
    @Override
    public EntityAgeable createChild(EntityAgeable ageable) {
        return new EntityPhoenix(world);
    }

}
