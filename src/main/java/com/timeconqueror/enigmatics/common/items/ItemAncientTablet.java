package com.timeconqueror.enigmatics.common.items;

import com.timeconqueror.enigmatics.client.gui.tablet.GUITablet;
import com.timeconqueror.enigmatics.utilities.WorldUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemAncientTablet extends Item {

    public ItemAncientTablet() {
        setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if (WorldUtils.isClient(worldIn)) {
            Minecraft.getMinecraft().displayGuiScreen(new GUITablet());
        }
        return new ActionResult<>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
    }
}
