package com.timeconqueror.enigmatics.common.tileentitites;

import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntityItemContainer extends TileEntity {

    private ItemStack item = ItemStack.EMPTY;

    @Override
    public void readFromNBT(NBTTagCompound tag) {
        super.readFromNBT(tag);
        if (tag.hasKey("Item")) {
            this.setItem(new ItemStack(tag.getCompoundTag("Item")));
        } else {
            this.setItem(ItemStack.EMPTY);
        }
    }

    @Nonnull
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound tag) {
        if (!item.isEmpty()) {
            tag.setTag("Item", item.serializeNBT());
        }
        return super.writeToNBT(tag);
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, this.getBlockMetadata(), this.getUpdateTag());
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void onDataPacket(NetworkManager connection, SPacketUpdateTileEntity packet) {
        this.readFromNBT(packet.getNbtCompound());
    }

    @Nonnull
    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = new NBTTagCompound();
        this.writeToNBT(tag);
        return tag;
    }

    @Override
    public void handleUpdateTag(@Nonnull NBTTagCompound tag) {
        this.readFromNBT(tag);
    }

    @Nonnull
    @Override
    public NBTTagCompound getTileData() {
        NBTTagCompound tag = new NBTTagCompound();
        this.writeToNBT(tag);
        return tag;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        //ItemStack prev = this.item;
        if (item.isEmpty()) {
            this.setRawItem(ItemStack.EMPTY);
        } else {
            ItemStack is = item.copy();
            is.setCount(1);
            this.setRawItem(is);
        }
        // if(prev != item){
        this.markDirty();
        if (this.world != null && !this.world.isRemote) {
            IBlockState state = this.world.getBlockState(this.pos);
            this.world.notifyBlockUpdate(pos, state, state, 3);
        }
        // }
    }

    public boolean hasItem() {
        return !item.isEmpty();
    }

    protected void setRawItem(ItemStack item) {
        this.item = item;
    }

    public float getItemRenderOffset() {
        return 0;
    }

    public float getItemSpawnOffset() {
        return 0.2F + getItemRenderOffset();
    }

    public boolean isAllowPutItem(ItemStack stack, TileEntityItemContainer tileEntity) {
        return true;
    }
}
