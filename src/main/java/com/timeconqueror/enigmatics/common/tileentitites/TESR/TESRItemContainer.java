package com.timeconqueror.enigmatics.common.tileentitites.TESR;

import com.timeconqueror.enigmatics.common.tileentitites.TileEntityItemContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class TESRItemContainer extends TileEntitySpecialRenderer<TileEntityItemContainer> {

    @Override
    public void render(TileEntityItemContainer tileEntity, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        if (tileEntity.hasItem()) {
            GL11.glPushMatrix();


            setupMainRender(tileEntity, x, y, z, partialTicks);
            setupLightmap(tileEntity);
            renderMain(tileEntity, x, y, z, partialTicks);

            GL11.glPopMatrix();
        }
        super.render(tileEntity, x, y, z, partialTicks, destroyStage, alpha);
    }

    protected void setupMainRender(TileEntityItemContainer tileEntity, double x, double y, double z, float partialTicks) {
        GL11.glTranslated(x + 0.5D, y + tileEntity.getItemRenderOffset(), z + 0.5D);
        GL11.glRotatef((tileEntity.getWorld().getTotalWorldTime() + partialTicks + tileEntity.getPos().hashCode() % 360) * 3F,
                0.0F, 1.0F, 0.0F);
    }

    protected void setupLightmap(TileEntityItemContainer tileEntity) {
        int i = 0;
        BlockPos itemPos = tileEntity.getPos().up();
        if (tileEntity.getWorld().isBlockLoaded(itemPos)) {
            i = tileEntity.getWorld().getCombinedLight(itemPos, 0);
        }
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) (i % 0x10000), (float) (i >> 16));
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    protected void renderMain(TileEntityItemContainer tileEntity, double x, double y, double z, float partialTicks) {
        Minecraft.getMinecraft().getRenderItem().renderItem(tileEntity.getItem(), ItemCameraTransforms.TransformType.GROUND);
    }
}