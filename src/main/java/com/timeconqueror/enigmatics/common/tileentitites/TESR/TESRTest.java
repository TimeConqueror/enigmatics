package com.timeconqueror.enigmatics.common.tileentitites.TESR;

import com.timeconqueror.enigmatics.common.tileentitites.TETest;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;

public class TESRTest extends TileEntitySpecialRenderer<TETest> {
    @Override
    public void render(TETest te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {

        Minecraft mc = Minecraft.getMinecraft();
//
//
//        Tessellator tessellator = Tessellator.getInstance();
//        BufferBuilder buf = tessellator.getBuffer();

        GlStateManager.enableTexture2D();
        mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);

        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.alphaFunc(516, 0.1F);
        GlStateManager.enableBlend();
        GlStateManager.disableLighting();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.pushMatrix();

        GlStateManager.color(1, 1, 1, 1);

        float scale = (float) (2 + 1 * Math.sin(Math.toRadians(System.currentTimeMillis() / 7 % 360)));

        GlStateManager.translate(x, y, z);
        GlStateManager.translate(0.5, 0.5, 0.5);
        GlStateManager.rotate(System.currentTimeMillis() / 9 % 360, 0, 1, 0);
        GlStateManager.scale(scale, scale, scale);
        GlStateManager.translate(-0.5, -0.5, -0.5);

//        IBlockState state = getWorld().getBlockState(te.getPos());
        IBlockState state = te.getBlockType().getDefaultState();

        Minecraft.getMinecraft().getRenderManager();

        BlockModelRenderer blockRenderer = mc.getBlockRendererDispatcher().getBlockModelRenderer();
//        buf.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);

//        GlStateManager.translate(-te.getPos().getX(), -te.getPos().getY(), -te.getPos().getZ());
//        te.getBlockType().getDefaultState()
//        blockRenderer.renderModelFlat()
        IBakedModel model = mc.getBlockRendererDispatcher().getModelForState(state);
        blockRenderer.renderModelBrightness(model, state, 1.0F, true);
//        blockRenderer.renderModel(getWorld(), mc.getBlockRendererDispatcher().getModelForState(state), state, BlockPos.ORIGIN, buf, false);
//        blockRenderer.renderModelBrightness(mc.getBlockRendererDispatcher().getModelForState(state), state, 15, true);
//        tessellator.draw();

        GlStateManager.popMatrix();
        GlStateManager.disableBlend();
        GlStateManager.enableLighting();

        super.render(te, x, y, z, partialTicks, destroyStage, alpha);
    }

    @Override
    public boolean isGlobalRenderer(TETest te) {
        return true;
    }

}
