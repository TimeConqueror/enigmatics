package com.timeconqueror.enigmatics;

import com.timeconqueror.enigmatics.registry.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CreativeTabE extends CreativeTabs {

    private static ItemStack tabIcon = new ItemStack(ModItems.ancientTablet);
    public static CreativeTabs enigmatics = new CreativeTabE(CreativeTabs.getNextID(), Enigmatics.MODID);

    public CreativeTabE(final int index, final String label) {
        super(index, label);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public ItemStack createIcon() {
        if (tabIcon == null)
            tabIcon = new ItemStack(ModItems.ancientTablet);
        return tabIcon;
    }
}
