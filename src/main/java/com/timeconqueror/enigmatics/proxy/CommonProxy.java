package com.timeconqueror.enigmatics.proxy;

import com.timeconqueror.enigmatics.common.world.gen.OverworldOreGenerator;
import com.timeconqueror.enigmatics.common.world.gen.TabletSphereGenerator;
import com.timeconqueror.enigmatics.registry.*;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent event){
        ModItems.register();
        ModBlocks.register();
        ModTileEntities.register();
        ModEntities.register();
    }

    public void init(FMLInitializationEvent event){
        GameRegistry.registerWorldGenerator(new OverworldOreGenerator(), 0);
        GameRegistry.registerWorldGenerator(new TabletSphereGenerator(), 100);
    }

    public void postInit(FMLPostInitializationEvent event){
        TabletRegistry.register();
    }
}
