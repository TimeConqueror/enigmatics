package com.timeconqueror.enigmatics.api.tablet;

import net.minecraft.util.ResourceLocation;

import java.util.LinkedHashMap;
import java.util.Map;

public class TabletAPI {
    public static Map<String, TabletCategory> categoryList = new LinkedHashMap<>();

    public static void registerCategory(String categoryName, ResourceLocation background) {
        registerCategory(categoryName, background, 2);
    }

    public static void registerCategory(String categoryName, ResourceLocation background, int categorySize) {
        TabletCategory tabletCategory = new TabletCategory(categoryName, background, categorySize);
        registerCategory(tabletCategory);
    }

    public static void registerCategory(TabletCategory tabletCategory) {
        categoryList.put(tabletCategory.getCategoryName(), tabletCategory);
    }

    public static void registerEntry(String existentCategoryName, TabletEntry tabletEntry) {
        if (existentCategoryName == null || !categoryList.containsKey(existentCategoryName)) {
            throw new IndexOutOfBoundsException("Attempt to register Tablet Entry \"" + tabletEntry.getId() + "\" to nonexistent Category \"" + existentCategoryName + "\".\n\tAvailable Category Names: " + categoryList.toString());
        }

        categoryList.get(existentCategoryName).addEntryToCategory(tabletEntry);
    }

    public static TabletCategory getCategoryByName(String categoryName) {
        categoryName = categoryName.toUpperCase();
        if (!categoryList.containsKey(categoryName)) {
            throw new IndexOutOfBoundsException("Attempt to get the nonexistent Category \"" + categoryName + "\".\n\tAvailable Category Names: " + categoryList.toString());
        }
        return categoryList.get(categoryName);
    }

    public static TabletCategory getFirstCategory() {
        Map.Entry<String, TabletCategory> entry = categoryList.entrySet().iterator().next();
        return entry.getValue();
    }

    public static TabletCategory getCategoryByIndex(int index) {
        Map.Entry<String, TabletCategory> category = categoryList.entrySet().stream().skip(index).findFirst().get();
        return category.getValue();
    }

    public static String getCategoryIdByIndex(int index) {
        Map.Entry<String, TabletCategory> category = categoryList.entrySet().stream().skip(index).findFirst().get();
        return category.getKey();
    }

    public static TabletEntry getEntryByIndex(TabletCategory category, int index) {
        Map.Entry<String, TabletEntry> entry = category.getEntryList().entrySet().stream().skip(index).findFirst().get();
        return entry.getValue();
    }

    public static int getIndexOfCategory(TabletCategory category) {
        for (int i = 0; i < categoryList.size(); i++) {
            if (category.equals(categoryList.entrySet().stream().skip(i).findFirst().get().getValue())) {
                return i;
            }
        }
        return -1;
    }
}
