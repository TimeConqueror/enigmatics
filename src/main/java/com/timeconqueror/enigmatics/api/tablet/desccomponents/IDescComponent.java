package com.timeconqueror.enigmatics.api.tablet.desccomponents;

public interface IDescComponent {
    Object getContentPart();

    String getType();

    int getContentPartHeight();

    void drawContentPart(int x0, int y0, int width);

    /**
     * Must be called before drawingContentPart and doing any mechanics with
     */
    void determineContentPartHeight(int width);
}
