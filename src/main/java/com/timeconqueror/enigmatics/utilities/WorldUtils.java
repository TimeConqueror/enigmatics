package com.timeconqueror.enigmatics.utilities;

import net.minecraft.world.World;

public class WorldUtils {

    public static boolean isClient(World world) {
        return world.isRemote;
    }

    public static boolean isServer(World world) {
        return !world.isRemote;
    }
}
