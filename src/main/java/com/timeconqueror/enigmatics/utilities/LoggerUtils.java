package com.timeconqueror.enigmatics.utilities;

import com.timeconqueror.enigmatics.Enigmatics;

public class LoggerUtils {

    public static void logFatal(String message) {
        Enigmatics.logger.fatal(message);
    }

    public static void logError(String message) {
        Enigmatics.logger.error(message);
    }

    public static void logInfo(String message) {
        Enigmatics.logger.info(message);
    }
}
